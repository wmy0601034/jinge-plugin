package com.caih.kinggrid_lib;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.caih.kinggrid_lib.base.BaseActivity;
import com.caih.kinggrid_lib.util.SPUtils;
import com.caih.kinggrid_lib.util.Utils;
import com.caih.kinggrid_lib.view.ConstantValue;
import com.caih.kinggrid_lib.view.adapter.AnnotAdapter;
import com.caih.kinggrid_lib.view.dialog.ProgressLayout;
import com.caih.kinggrid_lib.view.sign.util.BitmapUtil;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Author: wmy
 * Date: 2020/7/20 15:49
 */
public class KinggridActivity extends BaseActivity {

    private static String TAG = "KinggridActivity";

    public static boolean TAB_ENABLE = false; // 配置是否显示“切换编辑模式”的按钮 如该标记为false 则默认手笔分离

    private String downloadUrl = "";
    private String token = "";
    public static String userName = "";
    private String userId = "";
    private String copyRight = "";
    private String downloadPath = "";
    private String fileId = "";
    private String fileName = "";
    private String filePath = "";
    private String fileType = "";
    private String finalFileName = "";
    private String busiType = "";
    private String busiId = "";
    private String taskId = "";
    private String envServer = "";
    private long documentFileSize;
    private boolean editable = false;

    private LinearLayout llBack;
    private LinearLayout rlDelete;
    private LinearLayout rlTab;
    private LinearLayout rlConfig;
    private LinearLayout rlSave;
    private LinearLayout rlEdit;
    private LinearLayout rlKeyboard;
    private ImageView ivTab;
    private TextView tvTab;

    private LinearLayout llPrintAnnot;
    private TextView tvCommonAnnot;
    private EditText edAnnot;
    private TextView btnCancel;
    private TextView btnOk;

    private FrameLayout mFrameLayout;
    private LinearLayout mToolbar;
    private Context mContext;
    private View handwriteView_layout;
    private boolean isSavedHandwriting = false;
    public enum DeviceType{PHONE,PAD}
    private DeviceType mDeviceType;
    private boolean isSupportEbenT7Mode = true;
    private boolean isVectorSign = true;
    public static float densityCoefficient;
    public static final DisplayMetrics DM = new DisplayMetrics();
    private AlertDialog.Builder mSignatureReportBuilder;
    //是否是打字机注释
    private boolean isFreeText = false;
    // 下载进度
    private ProgressLayout progressDialog;
    private int SAVE_TYPE = ConstantValue.SAVE_TYPE_ON_CLICK_SAVE;
    private boolean clickCancelSave = false;
    private boolean processingBackPress = false;
    private boolean hasUploadToServer = false;
    private boolean ebHandWritingMode = true; // 是否为E人E本编辑模式  如果false则是全文批注/手写模式
    private boolean saveDocumentFromTab = false;    // 通过点切换编辑模式保存的文档

    private boolean initSuccess = false;
    private boolean hasUploadFailed = false;    // 是否曾保存失败
    private boolean hasDeleteAnnotion = false;  // 是否长按删除批注的标记
    private boolean hasInsertAnnotion = false;  // 是否插入过文字批注图片的标记

    private String SIGN_PIC_PATH;

    private String[] commonAnnots = ConstantValue.DEFAULT_COMMON_ANNOTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        //After LOLLIPOP not translucent status bar
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //Then call setStatusBarColor.
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        }
        mContext = this;
        SIGN_PIC_PATH = getExternalCacheDir().getPath()+ ConstantValue.SIGN_PIC_PATH_SUFFIX;
        ebHandWritingMode = SPUtils.getHandwriteMode(mContext) == ConstantValue.WRITE_MODE_PEN;
        getWindowManager().getDefaultDisplay().getMetrics(DM);
        densityCoefficient = DM.density;
        initView();
        initPDFParams();
        initProgressDialog();
    }

    private void initView(){
        mDeviceType = getDeviceType(this);
        if(Utils.isERenEBen()){
            KinggridActivity.TAB_ENABLE = false;
        }else {
            KinggridActivity.TAB_ENABLE = true;
        }
        if(mDeviceType == DeviceType.PAD){
            if(TAB_ENABLE) {
                rlTab.setVisibility(VISIBLE);
            }else {
                rlTab.setVisibility(GONE);
            }
            rlConfig.setVisibility(VISIBLE);
        }else {
            rlTab.setVisibility(GONE);
            rlConfig.setVisibility(GONE);
        }
    }


    private PopupWindow mSettingWindow;
    // 手写分离时的线粗细配置
    private void showSettingWindow(){

    }

    public void initProgressDialog(){
        progressDialog.show();
    }

    private void downloadDocument(){
//        String saveDir = Environment.getExternalStorageDirectory().getPath();
        String saveDir = getCacheDir().getPath();
        filePath = saveDir+"/"+finalFileName;
        Log.i(TAG, "开始下载 downloadUrl = "+downloadUrl);
        Log.i(TAG, "开始下载 filePath = "+filePath);
        downloadFile(downloadUrl, token, filePath, documentFileSize, new DownloadListener() {
            @Override
            public void onDownloadSuccess(String filePath) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });
            }

            @Override
            public void onDownloading(int progress) {

            }

            @Override
            public void onDownloadFailed() {
                progressDialog.dismiss();
            }
        });
    }






    /**
     * 输入文档密码Dialog
     */
    public void handlePassword() {

    }

    public void showDocumentDamaged() {

    }// end of showOpenDocumentFailed();

    public void showOpenDocumentFailed() {

    }// end of showOpenDocumentFailed();


    private void initPDFParams(){
        userName = getIntent().getStringExtra("userName");
        userId = getIntent().getStringExtra("userId");
        copyRight = getIntent().getStringExtra("copyRight");
        token = getIntent().getStringExtra("token");
        downloadPath = getIntent().getStringExtra("downloadPath");
        fileId = getIntent().getStringExtra("fileId");
        fileName = getIntent().getStringExtra("fileName");
        documentFileSize = getIntent().getLongExtra("fileSize", 0);
        fileType = getIntent().getStringExtra("fileType");
        busiType = getIntent().getStringExtra("busiType");
        busiId = getIntent().getStringExtra("busiId");
        envServer = getIntent().getStringExtra("envServer");
        editable = getIntent().getBooleanExtra("editable", false);
        taskId = getIntent().getStringExtra("taskId");
        if(!editable){
            rlSave.setVisibility(GONE);
            rlKeyboard.setVisibility(GONE);
            rlEdit.setVisibility(GONE);
            rlTab.setVisibility(GONE);
            rlConfig.setVisibility(GONE);
            rlDelete.setVisibility(GONE);
        }
        finalFileName = fileName + "."+fileType;
        downloadUrl = downloadPath+fileId;
    }

    private void updateEditMenu(boolean editing){
        if(editing){
            rlEdit.setVisibility(GONE);
            rlSave.setVisibility(VISIBLE);
            rlDelete.setVisibility(VISIBLE);
            if(TAB_ENABLE) {
                rlTab.setVisibility(VISIBLE);
            }
            if(mDeviceType == DeviceType.PAD) {
                rlConfig.setVisibility(VISIBLE);
            }
        }else {
            rlEdit.setVisibility(VISIBLE);
            rlTab.setVisibility(GONE);
            rlConfig.setVisibility(GONE);
            rlSave.setVisibility(GONE);
            rlDelete.setVisibility(GONE);
        }
    }

    @Override
    public void finish() {
        if(hasUploadToServer) {
            Intent intent = new Intent();
            Log.i(TAG, "=================>sendBroadCast");
            intent.setAction(ConstantValue.FINISH_ACTION);
            sendBroadcast(intent);
        }
        super.finish();
    }

    private String getUrlCncode(String content){
        try {
            return URLEncoder.encode(content,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return content;
        }
    }



    private byte charToByte(char c){
        return (byte)"0123456789ABCDEF".indexOf(c);
    }

    Handler myHandler = new Handler();


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    @Override
    protected void onPause() {
        super.onPause();
    }

    private DeviceType getDeviceType(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        // 屏幕尺寸
        double screenInches = Math.sqrt(x + y);

        // 大于7尺寸则为Pad
        if (screenInches >= 7.0) {
            return DeviceType.PAD;
        }
        return DeviceType.PHONE;
    }

    /**
     * 检查是否有本地签名
     */
    private void checkSignature(){
        String signPath = mContext.getExternalCacheDir().getPath()+ConstantValue.SIGN_PIC_PATH_SUFFIX;
        File file = new File(signPath);
        if(file.exists()){
            // 显示文本批注
            showPrintAnnotateDialog();
        }else {
            showNoSignatureDialog();
        }
    }

    /**
     * 显示文本批注框
     */
    private void showPrintAnnotateDialog(){
        llPrintAnnot.setVisibility(VISIBLE);
    }

    /**
     * 显示常用的批注
     */
    private void showCommonAnnotDialog(){
        AnnotAdapter annotAdapter = new AnnotAdapter(getApplicationContext(), commonAnnots);
        mSettingWindow.setFocusable(true);
        // 设置不允许在外点击消失
        mSettingWindow.setOutsideTouchable(false);
        int [] locations = new int[2];
        tvCommonAnnot.getLocationOnScreen(locations);
        mSettingWindow.showAtLocation(tvCommonAnnot, Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM, 0, -locations[1]);
    }

    /**
     * 将输入的文字批注转换为图片 插入到PDF中 图片长度定为屏幕宽度的 4：9
     * 一行最多16个字 正常时8个字 每个字20dp
     * @param text
     */
    private void insertPrintAnnot(final String text){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap annotBm = BitmapUtil.text2AnnotBitmap(text, mContext);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "annotBm " +annotBm.getWidth()+" "+annotBm.getHeight());
                    }
                });
            }
        }).start();
    }

    private void showNoSignatureDialog(){
        AlertDialog signatureDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_LIGHT)
                .setCancelable(false)
                .setTitle("提示")
                .setMessage("当前没有签名，需要先在签名管理中录入签名")
                .setNegativeButton("取消", null)
                .setPositiveButton("前往签名", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(mContext, MySignActivity.class);
                        intent.putExtra("cookie", token);
                        intent.putExtra("envServer", envServer);
                        startActivity(intent);
                    }
                });
        signatureDialog = builder.create();
        signatureDialog.show();
    }

    /**
     * 隐藏键盘
     * 弹窗弹出的时候把键盘隐藏掉
     */
    protected void hideInputKeyboard() {
        if(this.getCurrentFocus()!=null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}
