package com.caih.kinggrid_lib.base;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.caih.kinggrid_lib.util.DownloadUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Author: wmy
 * Date: 2020/7/22 16:10
 */
public class BaseActivity extends Activity {

    protected String TAG = getClass().getSimpleName();
    private Handler mainHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void downloadFile(String downloadUrl, String token, String savePath, long fileSize, DownloadListener listener){
       
    }

    protected void toast(String msg) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public interface UploadListener{
        void onFailure(Exception e);
        void onSuccess();
    }

    public interface DownloadListener{
        void onDownloadSuccess(String filePath);
        void onDownloading(int progress);
        void onDownloadFailed();
    }


    public interface RequestDataListener{
        void onSuccess(String response);
        void onFailure(Exception e);
    }

}
