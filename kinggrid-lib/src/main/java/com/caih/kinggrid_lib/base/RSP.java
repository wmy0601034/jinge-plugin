package com.caih.kinggrid_lib.base;


/**
 * Author: wmy
 * Date: 2020/7/29 9:15
 */
public class RSP<E> {

    public static String baseSuccessRsp(){
        return "";
    }

    public static String baseFailedRsp(){
        return "";
    }

    public static String baseFailedRsp(String codeMsg){
        return "";
    }

    public static String failedRsp(String code, String codeMsg){
        return "";
    }

    public static<E> String baseSuccessRsp(String codeMsg){
        return baseSuccessRsp("0000", codeMsg, null);
    }


    public static<E> String baseSuccessRsp(String code, String codeMsg, E e){
        return "";
    }

}
