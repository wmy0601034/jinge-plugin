package com.caih.kinggrid_lib.view.sign;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.caih.kinggrid_lib.view.sign.config.PenConfig;


/**
 * 画笔设置窗口
 *
 * @author king
 * @since 2018-06-04
 */
public class PaintSettingWindow extends PopupWindow {
    public static final String[] PEN_COLORS = new String[]{"#101010", "#027de9", "#0cba02", "#f9d403", "#ec041f"};
    public static final int[] PEN_SIZES = new int[]{5, 15, 20, 25, 30};

    private Context context;
    private CircleView lastSelectColorView;
    private CircleView lastSelectSizeView;
    private int selectColor;
    private View rootView;
    private OnSettingListener settingListener;

    public PaintSettingWindow(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {


    }

    /**
     * 显示在左上角
     */
    public void popAtTopLeft() {

    }

    /**
     * 显示在右上角
     */
    public void popAtTopRight() {

    }

    /**
     * 显示在右下角
     */
    public void popAtBottomRight() {

    }

    /**
     * 显示在左边
     */
    public void popAtLeft() {

    }

    public interface OnSettingListener {
        void onColorSetting(int color);

        void onSizeSetting(int index);
    }

    public void setSettingListener(OnSettingListener settingListener) {
        this.settingListener = settingListener;
    }
}
