package com.caih.kinggrid_lib.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.caih.kinggrid_lib.KinggridActivity;
import com.caih.kinggrid_lib.MySignActivity;
import com.caih.kinggrid_lib.base.BaseActivity;
import com.caih.kinggrid_lib.base.RSP;
import com.caih.kinggrid_lib.http.API;
import com.caih.kinggrid_lib.view.dialog.ProgressLayout;
import com.caih.kinggrid_lib.view.sign.util.SignUtils;
import com.facebook.react.bridge.Callback;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;


/**
 * Author: wmy
 * Date: 2020/7/17 15:12
 */
public class RCTDocumentView extends LinearLayout {

    private String TAG = "RCTDocumentView";
    private Context mContext;
    private FrameLayout mFrameLayout;

    private static String mFilePath = "";
    public static String userName = "";
    public static String userId = "";
    public static String copyRight = "";

    private LinearLayout rlDelete;
    private LinearLayout rlTab;
    private LinearLayout rlSave;
    private LinearLayout rlConfig;
    private LinearLayout rlEdit;
    private LinearLayout rlKeyboard;
    private ImageView ivTab;
    private TextView tvTab;

    private LinearLayout llPrintAnnot;
    private TextView tvCommonAnnot;
    private EditText edAnnot;
    private TextView btnCancel;
    private TextView btnOk;

    private String downloadUrl = "";
    private String token = "";
    private String downloadPath = "";
    private String fileId = "";
    private String fileName = "";
    private long fileSize = 0l;
    private String filePath = "";
    private String fileType = "";
    private String finalFileName = "";
    private String busiType = "";
    private String busiId = "";
    private String taskId = "";
    private boolean editable = false;
    private String envServer = "";

    public static final DisplayMetrics DM = new DisplayMetrics();

    public enum DeviceType{PHONE,PAD}
    private DeviceType mDeviceType;

    public static float densityCoefficient;

    private boolean isSupportEbenT7Mode = true;
    private boolean isVectorSign = true;

    //是否是打字机注释
    private boolean isFreeText = false;

    private ProgressLayout progressDialog;
    private AlertDialog.Builder mSignatureReportBuilder;
    private boolean isSavedHandwriting = false;
    private boolean initSuccess = false;
    private Callback modifiedCallback;
    private Callback uploadCallback;
    private boolean saveAndUploading = false;
    private boolean clickCancelSave = false;
    private boolean processingBackPress = false;
    private int width = getWidth();
    private int height = getHeight();
    private boolean ebHandWritingMode = true; // 是否为E人E本编辑模式  如果false则是全文批注/手写模式
    private boolean saveDocumentFromTab = false;    // 通过点切换编辑模式保存的文档
    private boolean hasUploadFailed = false;    // 是否曾保存失败
    private boolean hasDeleteAnnotion = false;  // 是否长按删除批注的标记
    private boolean hasInsertAnnotion = false;  // 是否插入过文字批注图片的标记

    private int SAVE_TYPE = ConstantValue.SAVE_TYPE_ON_CLICK_SAVE;

    private String SIGN_PIC_PATH;
    private String[] commonAnnots = ConstantValue.DEFAULT_COMMON_ANNOTS;

    public RCTDocumentView(Context context) {
        super(context);
        this.mContext = context;
        init();
    }

    public RCTDocumentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    public RCTDocumentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        init();
    }

//    private static RCTDocumentView ourInstance;
//
//    public static RCTDocumentView getInstance() {
//        return ourInstance;
//    }
//    public void createInstance() {
//        if(ourInstance == null) {
//            ourInstance = this;
//        }
//    }

    public void destoryInstance(){

    }

    private void init() {}

    private void save(){}

    private PopupWindow mSettingWindow;
    // 手写分离时的线粗细配置
    private void showSettingWindow(){
    }

    public void initProgressDialog(){

    }

    public void setParams(String params){}

    public void downloadDocument(){}

    private void initSign(){
        File file = new File(SIGN_PIC_PATH);
        if(file.exists()) {
            getCommonAnnot();
        }else {
            getSignInfo();
        }
    }

    private void getCommonAnnot(){}

    private void getSignInfo(){}

    private void downloadSign(String signatureUrl){}

    public void initDocument(){}

    @Override
    public void requestLayout() {
        super.requestLayout();
        post(measureAndLayout);
    }

    private final Runnable measureAndLayout = new Runnable() {
        @Override
        public void run() {
            measure(
                    MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
            layout(getLeft(), getTop(), getRight(), getBottom());
        }
    };

    public void openHandWrite(){}

    Handler mHandler = new Handler(){
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
        }
    };

    public void isDocumentModified(Callback callback){
        this.modifiedCallback = callback;
        if(processingBackPress){
            return;
        }
        processingBackPress = true;
        if(!initSuccess){
            processingBackPress = false;
            modifiedInvoke(true);
//            destoryInstance();
            return;
        }
        if(hasDeleteAnnotion){
            showSaveDialog();
            return;
        }
        if(hasUploadFailed){
            showSaveDialog();
            return;
        }
        if(hasInsertAnnotion){
            showSaveDialog();
            return;
        }
        if(rlSave.getVisibility() == GONE){
            processingBackPress = false;
            modifiedInvoke(true);
//            destoryInstance();
            return;
        }
        SAVE_TYPE = ConstantValue.SAVE_TYPE_ON_CLICK_BACK;
        // 如果已经编辑过 提示是否保存 未编辑 直接退出
        try {
        }catch (Exception e){
            modifiedInvoke(false);
            Log.i(TAG, "587 "+e.toString());
        }
    }

    /**
     * 自动保存并上传
     */
    public void autoSaveAndUpload(Callback callback){
        if(!initSuccess){
            callback.invoke(RSP.baseSuccessRsp("未打开文档"));
            return;
        }
        if(saveAndUploading){
            callback.invoke(RSP.baseFailedRsp("正在上传"));
            return;
        }
        SAVE_TYPE = ConstantValue.SAVE_TYPE_ON_CLICK_SUBMIT;
        this.uploadCallback = callback;
    }

    private void updateEditMenu(boolean editing){
        if(editing){
            rlEdit.setVisibility(View.GONE);
            rlSave.setVisibility(View.VISIBLE);
            rlDelete.setVisibility(View.VISIBLE);
            if(KinggridActivity.TAB_ENABLE) {
                rlTab.setVisibility(View.VISIBLE);
            }
            if(mDeviceType == DeviceType.PAD) {
                rlConfig.setVisibility(VISIBLE);
            }
        }else {
            rlEdit.setVisibility(View.VISIBLE);
            rlTab.setVisibility(GONE);
            rlConfig.setVisibility(GONE);
            rlSave.setVisibility(View.GONE);
            rlDelete.setVisibility(View.GONE);
        }
    }

    /*
     * 显示保存提示框
     */
    private void showSaveDialog(){
        clickCancelSave = false;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_LIGHT);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                processingBackPress = false;
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }

    private void initEb(){
    }

    private void openEBHandwriteView(){}

    public void preReInitDocument(){}

    private boolean add = false;
    /**
     * 将文档视图添加到界面上，设置相关参数，监听事件等
     */
    public void drawView(){}

    private void uploadDocument(){
        String uploadUrl = API.getAPI(envServer, API.API_UPLOAD_FILE);
        Log.i(TAG, "uploadDocument "+uploadUrl);
        String fileNameWithType = fileName+"."+fileType;
        String name = fileNameWithType;
        fileNameWithType = getUrlCncode(fileNameWithType);
        HashMap<String, Object> params = new HashMap<>();
        params.put("busiType", busiType);
        params.put("name", name);
        params.put("id", fileId);
        params.put("busiId", busiId);
        params.put("taskId", taskId);
        uploadForUnpdateFile(uploadUrl, filePath, fileNameWithType, token, params, new BaseActivity.UploadListener() {
            @Override
            public void onFailure(Exception e) {
                Log.i(TAG, "uploadDocument onFailure "+e.toString());
                if(SAVE_TYPE == ConstantValue.SAVE_TYPE_ON_CLICK_SAVE) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            processingBackPress = false;
                            hasUploadFailed = true;
                            // 此时要重新进入编辑模式
                            openHandWrite();
                        }
                    });
                }else {
                    uploadInvoke(RSP.baseFailedRsp(e.toString()));
                    saveAndUploading = false;
                }
                modifiedInvoke(false);
            }

            @Override
            public void onSuccess() {
                Log.i(TAG, "uploadDocument onSuccess ");
                hasDeleteAnnotion = false;
                hasInsertAnnotion = false;
                hasUploadFailed = false;
                if(SAVE_TYPE == ConstantValue.SAVE_TYPE_ON_CLICK_SAVE){
                    hideLoading();
                    updateEditMenu(false);
                }else {
                    if (SAVE_TYPE == ConstantValue.SAVE_TYPE_ON_CLICK_BACK) {
                        hideLoading();
                        processingBackPress = false;
                        modifiedInvoke(true);
//                        destoryInstance();
                    } else {
                        uploadInvoke(RSP.baseSuccessRsp());
                        saveAndUploading = false;
//                        destoryInstance();
                    }
                }
            }
        });
    }

    private void modifiedInvoke(boolean result){
        if(modifiedCallback!=null){
            modifiedCallback.invoke(result);
            modifiedCallback = null;
        }
    }

    private void uploadInvoke(String rsp){
        if(uploadCallback!=null){
            uploadCallback.invoke(rsp);
            uploadCallback = null;
        }
    }

    private String getUrlCncode(String content){
        try {
            return URLEncoder.encode(content,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return content;
        }
    }

    protected void uploadForUnpdateFile(String url, String filePath, String fileName, String token, HashMap<String, Object> params, final BaseActivity.UploadListener listener){}

    private DeviceType getDeviceType() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        // 屏幕尺寸
        double screenInches = Math.sqrt(x + y);

        // 大于7尺寸则为Pad
        if (screenInches >= 7.0) {
            return DeviceType.PAD;
        }
        return DeviceType.PHONE;
    }

    private void showSignatureDialog(String message){
        mSignatureReportBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_LIGHT);
        mSignatureReportBuilder.setTitle("验证结果");
        mSignatureReportBuilder
                .setPositiveButton("确定",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog report = mSignatureReportBuilder.create();
        if (message != null) {
            report.setMessage(message);
        } else {
            report.setMessage("验证文档无效，文档已被更改或损坏！");
        }

        report.show();
    }

    /**
     * 输入文档密码Dialog
     */
    public void handlePassword() {
        final EditText passwordText = new EditText(mContext);
        passwordText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_PASSWORD );
        passwordText.setTransformationMethod(new PasswordTransformationMethod());
        AlertDialog.Builder passwordEntryBuilder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_LIGHT);
        passwordEntryBuilder.setView(passwordText);
        AlertDialog passwordEntry = passwordEntryBuilder.create();
//		passwordEntry.getWindow().setSoftInputMode(
//		    WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        passwordEntry.show();
    }

    public void showOpenDocumentFailed() {
        TextView passwordErrorView = new TextView(mContext);
        passwordErrorView.setTextColor(Color.BLACK);
        passwordErrorView.setHeight(100);
        passwordErrorView.setGravity(Gravity.CENTER);


    }

    public void showDocumentDamaged() {


    }

    /**
     * 检查是否有本地签名
     */
    private void checkSignature(){
        String signPath = SignUtils.getSignPath(mContext);
        File file = new File(signPath);
        if(file.exists()){
            // 显示文本批注
            showPrintAnnotateDialog();
        }else {
            showNoSignatureDialog();
        }
    }

    /**
     * 显示文本批注框
     */
    private void showPrintAnnotateDialog(){
        llPrintAnnot.setVisibility(VISIBLE);
    }

    /**
     * 显示常用的批注
     */
    private void showCommonAnnotDialog(){
        mSettingWindow.setFocusable(true);
        // 设置不允许在外点击消失
        mSettingWindow.setOutsideTouchable(false);
        int [] locations = new int[2];
        tvCommonAnnot.getLocationOnScreen(locations);
        mSettingWindow.showAtLocation(tvCommonAnnot, Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM, 0, -locations[1]);
    }

    private void showNoSignatureDialog(){
        AlertDialog signatureDialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, AlertDialog.THEME_HOLO_LIGHT)
                .setCancelable(false)
                .setTitle("提示")
                .setMessage("当前没有签名，需要先在签名管理中录入签名")
                .setNegativeButton("取消", null)
                .setPositiveButton("前往签名", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(mContext, MySignActivity.class);
                        intent.putExtra("cookie", token);
                        intent.putExtra("envServer", envServer);
                        mContext.startActivity(intent);
                    }
                });
        signatureDialog = builder.create();
        signatureDialog.show();
    }

    private void hideLoading(){
        progressDialog.dismiss();
    }

    private void toast(int resId){
        Toast.makeText(mContext, resId, Toast.LENGTH_SHORT).show();
    }

    private void toast(String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    /**
     * 隐藏键盘
     * 弹窗弹出的时候把键盘隐藏掉
     */
    protected void hideInputKeyboard() {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
