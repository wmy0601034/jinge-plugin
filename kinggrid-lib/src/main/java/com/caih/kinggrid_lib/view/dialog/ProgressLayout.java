package com.caih.kinggrid_lib.view.dialog;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;



/**
 * Author: wmy
 * Date: 2020/8/4 8:56
 */
public class ProgressLayout extends LinearLayout {

    private Context mContext;
    private LinearLayout llContainer;
    private TextView tvMessage;

    public ProgressLayout(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public ProgressLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public ProgressLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init(){

    }

    public void setMessage(String message){
        tvMessage.setText(message);
    }

    public void show(){
        setVisibility(VISIBLE);
    }

    public void dismiss(){
        setVisibility(GONE);
    }

}
